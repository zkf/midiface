module Example exposing (..)

import Expect exposing (Expectation)
import Fuzz exposing (Fuzzer, int, list, string)
import Test exposing (..)
import Microbrute as Microbrute
import Midi as Midi
import Bytes.Encode as Encode
import ByteList exposing (toByteList)
import Bytes.Decode as Decode


encodeUint8List l = Encode.encode <| Encode.sequence <|
                  List.map Encode.unsignedInt8 l


suite : Test
suite =
  describe "MIDI tests"
    [ test "decode identify reply" <| \_ ->
          let input = encodeUint8List
                    [0xf0, 0x7e, 0x01, 0x06, 0x02, 0x00, 0x20, 0x6b, 0x04, 0x00, 0x02, 0x01, 0x01, 0x00, 0x03, 0x02, 0xf7]
              output = Decode.decode Microbrute.decode_ input
              expected = Just (Midi.IdentityReply
                { channel = Just 1
                , manufacturer = Midi.Arturia
                , device = Midi.Microbrute
                , version = Midi.Version 1 0 3 2
                })
          in
          Expect.equal output expected
      , test "encode set parameter" <| \_ ->
          let expected = Just [0xf0, 0x00, 0x20, 0x6b, 0x05, 0x01, 0x00, 0x01, 0x0b, 0x02, 0xf7]
              input = Midi.Sysex <|
                  -- Set note priority to "high
                  Microbrute.SetParameterValue
                    Microbrute.KeyboardNotePriority_
                    (Microbrute.KeyboardNotePriorityValue Microbrute.High)

              output = Maybe.map Encode.encode (Microbrute.encode_ input)
          in Expect.equal (Maybe.andThen toByteList output) expected
      ,  test "decode parameter value reply" <| \_ ->
            let
                input
                    =
                      encodeUint8List
                          [ 0xf0, 0x0, 0x20, 0x6b
                          , 0x5, 0x1, 0x0, 0x1
                          , 0xb, 0x0, 0x0, 0x0
                          , 0x0, 0x0, 0x0, 0x0
                          , 0x0, 0x0, 0xf7]
                output = Decode.decode Microbrute.decode_ input
                expected
                    = Just
                    << Midi.Sysex
                    <| Microbrute.SetParameterValue
                        Microbrute.KeyboardNotePriority_
                        (Microbrute.KeyboardNotePriorityValue Microbrute.Last)
            in Expect.equal output expected
      ]

