async function midiInit(send, subscribe) {
  const midiAccess = await navigator.requestMIDIAccess({ sysex: true })
  const initialInputs = midiAccess.inputs.values()
  const initialOutputs = midiAccess.outputs.values()
  const midiMap = {}

  function sendPort(port) {
    midiMap[port.id] = port
    const message = jsonify(port)
    send(message)
    console.log('sent port message to Elm', message)
  }

  function* multiIter(iters) {
    for (const iter of iters) {
      for (const value of iter) {
        yield value
      }
    }
  }
  for (const port of multiIter([initialInputs, initialOutputs])) {
    sendPort(port)
  }

  midiAccess.onstatechange = event => {
    sendPort(event.port)
  }

  subscribe(message => {
    console.log('received message from Elm', JSON.stringify(message))
    const port = midiMap[message.to]
    if (!port) {
      console.error(`invalid port id ${message.to}`)
      return
    }
    if (message.data) {
      if (port.type !== 'output') {
        console.error('port type must be output')
        return
      }
      console.log('sending MIDI message', message)
      port.send(message.data /*, timestamp (delay sending of message) */)
      navigator.vibrate(60)
    } else {
      if (port.type !== 'input') {
        console.error('port type must be input')
        return
      }
      if (port.onmidimessage) {
        console.error(`already subscribed to port ${port.id}`)
        return
      }
      port.onmidimessage = event => {
        // Filter out Active Sensing messages
        if (event.data[0] !== 0xFE) {
          send({
            // type: 'midiMessage',
            from: port.id,
            data: Array.from(event.data)
          })
          console.log('sent midi message to Elm', event)
        }
      }
      console.log('subscribed to messages from', message.to)
    }
  })

  function jsonify({ connection, id, manufacturer, name, state, type, version }) {
    return { connection, id, manufacturer, name, state, type, version }
  }
}
