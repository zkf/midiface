module Midi exposing (..)
import Bytes.Decode as Decode exposing (Decoder)
import Bytes.Encode as Encode exposing (Encoder)
import Bytes exposing (Endianness(..))

type Version = Version Int Int Int Int

versionToString (Version major minor point bug) =
  "v" ++ (String.join "." <| List.map String.fromInt [major, minor, point, bug])

type Device
  = Microbrute

deviceToString _ = "Microbrute"

type GenericMidiMessage sysex
  = IdentityRequest (Maybe Int) -- Maybe channel
  | IdentityReply
      { manufacturer : Manufacturer
      , device : Device
      , version: Version
      , channel: Maybe Int
      }
  | LocalControl OnOff
  | Sysex sysex


identityRequest = IdentityRequest Nothing

type OnOff
    = On
    | Off

encodeMidiMessage : (sysex -> Maybe Encoder) -> GenericMidiMessage sysex -> Maybe Encoder
encodeMidiMessage sysexEncoder m =
  let enc x = Encode.sequence (List.map Encode.unsignedInt8 x) in
  case m of
    IdentityRequest channel -> Just <| enc [0xf0, 0x7e, Maybe.withDefault 0x7f channel, 0x06, 0x01, 0xf7]
    Sysex x -> sysexEncoder x
    _ -> Nothing


decodeMidiMessage : (Manufacturer -> Decoder sysex) -> Decoder (GenericMidiMessage sysex)
decodeMidiMessage getDecoder =
  let
      sysexDecoder firstByte = decodeManufacturerID firstByte |> Decode.andThen getDecoder
  in
      decodeMessageType
          |> Decode.andThen (decodeMessage sysexDecoder)


decodeMessageType : Decoder MessageType
decodeMessageType =
  let helper type_ =
        case type_ of
          0xF0 -> Decode.succeed SysexMessage
          _ -> Decode.fail
  in
    oneByte |> Decode.andThen helper


decodeMessage : (Int -> Decoder sysex) -> MessageType -> Decoder (GenericMidiMessage sysex)
decodeMessage sysexDecoder type_ =
    case type_ of
        SysexMessage ->
            oneByte
            |> Decode.andThen (\firstByte ->
                case firstByte of
                    0x7E -> decodeUniversalSysex
                    0x7F -> Decode.fail -- Universal Realtime
                    _ -> Decode.map Sysex (sysexDecoder firstByte)
              )


decodeUniversalSysex =
  decodeSysexChannel
  |> Decode.andThen (\channel ->
    twoBytes
    |> Decode.andThen (universalSysex channel)
  )

decodeSysexChannel : Decoder (Maybe Int)
decodeSysexChannel = oneByte |> Decode.andThen (\ch -> Decode.succeed (if ch == 0x7F then Nothing else Just ch))

universalSysex channel subType =
  case subType of
    0x0601 -> Decode.succeed <| IdentityRequest channel
    0x0602 -> decodeIdentityReply channel
    _ -> Decode.fail

decodeIdentityReply : Maybe Int -> Decoder (GenericMidiMessage a)
decodeIdentityReply channel =
  oneByte |> Decode.andThen
  decodeManufacturerID |> Decode.andThen (\manuf ->
  decodeDeviceModel manuf |> Decode.andThen (\model ->
  decodeDeviceVersion |> Decode.andThen (\version ->
    Decode.succeed <|
      IdentityReply
        { manufacturer = manuf
        , device = model
        , version = version
        , channel = channel
        }
  )))

decodeDeviceModel manufacturer =
  case manufacturer of
    Arturia -> Decode.unsignedInt32 BE |> Decode.andThen (\x ->
      case x of
        0x04000201 -> Decode.succeed Microbrute
        _ -> Decode.fail
      )

decodeDeviceVersion = Decode.map4 Version oneByte oneByte oneByte oneByte

type MessageType = SysexMessage

type UniversalSysex = NonRealTime
type SysexType = Universal UniversalSysex | DeviceSpecific

type Manufacturer = Arturia

manufacturerToString Arturia = "Arturia"

getManufacturer : Int -> Decoder Manufacturer
getManufacturer id =
  case id of
    0x206B -> Decode.succeed Arturia
    _ -> Decode.fail

decodeManufacturerID : Int -> Decoder Manufacturer
decodeManufacturerID firstByte =
    if firstByte == 0x00
      then
          twoBytes
              |> Decode.andThen getManufacturer
      else getManufacturer firstByte

oneByte = Decode.unsignedInt8

twoBytes = Decode.unsignedInt16 BE
