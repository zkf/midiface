module ByteList exposing (toByteList, fromByteList)

import Bytes.Encode as Encode
import Bytes.Decode as Decode exposing (Decoder)
import Bytes as Bytes exposing (Bytes)

fromByteList : List Int -> Bytes
fromByteList = List.map Encode.unsignedInt8 >> Encode.sequence >> Encode.encode

toByteList : Bytes -> Maybe (List Int)
toByteList b = Decode.decode (list Decode.unsignedInt8 (Bytes.width b)) b

list : Decoder a -> Int -> Decoder (List a)
list decoder len =
    Decode.loop (len, []) (listStep decoder)

listStep : Decoder a -> (Int, List a) -> Decoder (Decode.Step (Int, List a) (List a))
listStep decoder (n, xs) =
    if n <= 0 then
        Decode.succeed (Decode.Done <| List.reverse xs)
    else
        Decode.map (\x -> Decode.Loop (n - 1, x :: xs)) decoder
