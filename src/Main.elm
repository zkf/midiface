port module Main exposing (main)

import Tuple
import ByteList exposing (toByteList, fromByteList)
import Browser
import Dict exposing (Dict)
import Html
    exposing
        ( Html
        , button
        , div
        , fieldset
        , input
        , label
        , legend
        , option
        , pre
        , select
        , span
        , text
        )
import Html.Attributes as Attr exposing (checked, name, style, type_, value)
import Html.Events exposing (onClick, onInput)
import Html.Events.Extra exposing (onChange)
import Json.Decode as Decode exposing (Decoder)
import Json.Decode.Pipeline exposing (required)
import Json.Encode as Encode exposing (Value)
import Microbrute as Microbrute
    exposing
        ( Setting (..)
        , SettingsGroup
        , ParameterValue
        , optionName
        , optionValue
        , updateSetting
        )
import Midi



-- MAIN


main : Program () Model Msg
main =
    Browser.element
        { init = init
        , update = update
        , view = view
        , subscriptions = subscriptions
        }


type MidiPortType
    = Input
    | Output


type alias MidiPort =
    { name : String
    , manufacturer : String
    , id : PortID
    , type_ : MidiPortType
    }


type alias MidiMessage =
    { data : List Int
    , from : PortID
    }


type PortID
    = PortID String


portIdToString : PortID -> String
portIdToString (PortID s) =
    s


type MidiMessageIn
    = MidiMessagePort MidiPort
    | MidiMessageMessage MidiMessage


type alias MidiSend =
    { to : PortID
    , data : List Int -- bytearray, uint8array
    }


type alias MidiSubscribe =
    { to : PortID }


type MidiMessageOut
    = MidiMessageSend MidiSend
    | MidiMessageSubscribe MidiSubscribe


midiMessageOutEncoder : MidiMessageOut -> Value
midiMessageOutEncoder msg =
    case msg of
        MidiMessageSend midiSend ->
            midiSendEncoder midiSend

        MidiMessageSubscribe midiSubscribe ->
            midiSubscribeEncoder midiSubscribe


midiSubscribeEncoder : MidiSubscribe -> Value
midiSubscribeEncoder { to } =
    Encode.object [ ( "to", portEncoder to ) ]


midiSendEncoder : MidiSend -> Value
midiSendEncoder { to, data } =
    Encode.object
        [ ( "to", portEncoder to )
        , ( "data", Encode.list Encode.int data )
        ]


portEncoder : PortID -> Value
portEncoder (PortID midiPort) =
    Encode.string midiPort


port midiReceiver : (Value -> msg) -> Sub msg


port midiSender : Value -> Cmd msg


subscriptions : Model -> Sub Msg
subscriptions _ =
    midiReceiver (\s -> decode s |> toMsg)


decode : Value -> Result Decode.Error MidiMessageIn
decode x =
    Decode.decodeValue midiMessageInDecoder <| x


toMsg : Result Decode.Error MidiMessageIn -> Msg
toMsg v =
    case v of
        Err err ->
            Error (Debug.toString err)

        Ok (MidiMessagePort x) ->
            Port x

        Ok (MidiMessageMessage x) ->
            Message x


midiMessageInDecoder : Decoder MidiMessageIn
midiMessageInDecoder =
    Decode.oneOf
        [ Decode.map MidiMessageMessage messageDecoder
        , Decode.map MidiMessagePort portDecoder
        ]


messageDecoder : Decoder MidiMessage
messageDecoder =
    Decode.succeed MidiMessage
        |> required "data" (Decode.list Decode.int)
        |> required "from" idDecoder


portDecoder : Decoder MidiPort
portDecoder =
    Decode.succeed MidiPort
        |> required "name" Decode.string
        |> required "manufacturer" Decode.string
        |> required "id" idDecoder
        |> required "type" typeDecoder


idDecoder : Decoder PortID
idDecoder =
    Decode.map PortID Decode.string


typeDecoder : Decoder MidiPortType
typeDecoder =
    Decode.string |> Decode.andThen toPort


toPort : String -> Decoder MidiPortType
toPort t =
    case t of
        "input" ->
            Decode.succeed Input

        "output" ->
            Decode.succeed Output

        _ ->
            Decode.fail "invalid port type"



-- MODEL


type alias Model =
    { ports : List MidiPort
    , selectedOutputPort : Maybe MidiPort
    , selectedInputPort : Maybe MidiPort
    , messages : Dict (List Int) MidiMessage
    , errors : List String
    , counter : Int
    , settings : List Microbrute.SettingsGroup
    , midiDevice : Maybe String
    }


initialModel : Model
initialModel =
      { ports = []
      , selectedOutputPort = Nothing
      , selectedInputPort = Nothing
      , messages = Dict.empty
      , errors = []
      , counter = 0
      , settings = Microbrute.settings
      , midiDevice = Nothing
      }


init : () -> ( Model, Cmd Msg )
init _ =
    ( initialModel, Cmd.none )



-- UPDATE


type Msg
    = Increment
    | Decrement
    | Error String
    | Port MidiPort
    | Message MidiMessage
    | SetOpt ParameterValue
    | SelectOutputMidiPort MidiPort
    | SelectInputMidiPort MidiPort
    | NoOp

midiToMicrobrute : MidiMessage -> Maybe Microbrute.MidiMessage
midiToMicrobrute { data } =
  let
      bytes = fromByteList data
  in
      Microbrute.decode bytes



update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        NoOp ->
            ( model, Cmd.none )

        Increment ->
            ( { model | counter = model.counter + 1 }, Cmd.none )

        Decrement ->
            ( { model | counter = model.counter - 1 }, Cmd.none )

        Error x ->
            ( { model | errors = x :: model.errors }, Cmd.none )

        Port x ->
          let
              selectedOutputPort =
                if x.type_ == Output && x.manufacturer == "Arturia" && x.name == "MicroBrute MIDI 1"
                    then Just x
                    else Nothing

              selectedInputPort =
                if x.type_ == Input && x.manufacturer == "Arturia" && x.name == "MicroBrute MIDI 1"
                    then Just x
                    else Nothing

          in
              { model | ports = x :: model.ports}
                |> selectInputMidiPort selectedInputPort
                |> \(model1, cmd) -> selectOutputMidiPort selectedOutputPort model1
                |> \(model2, cmd1) -> (model2, Cmd.batch [cmd, cmd1])

        Message x ->
          let
              decodedMessage = midiToMicrobrute x
          in
            ( case decodedMessage of
                Just (Midi.IdentityReply {manufacturer, device, version}) ->
                  { model | midiDevice =
                      Just <| String.join " "
                        [ Midi.manufacturerToString manufacturer
                        , Midi.deviceToString device
                        , Midi.versionToString version
                        ]
                  }
                Just (Midi.Sysex (Microbrute.SetParameterValue _ value)) ->
                    { model | settings = updateSetting model.settings value }
                _ ->
                  { model | messages = Dict.insert x.data x model.messages }
            , case decodedMessage of
                Just (Midi.IdentityReply {manufacturer, device}) ->
                  if manufacturer == Midi.Arturia && device == Midi.Microbrute
                    then
                      Maybe.map
                        (\midiPort -> model.settings
                          |> List.concatMap Tuple.second
                          |> List.filter (\(Setting{selected}) -> isNothing selected)
                          |> List.map (\(Setting{parameter}) -> parameter)
                          |> flip getParametersCmd midiPort
                        )
                        model.selectedOutputPort
                        |> Maybe.withDefault Cmd.none
                    else Cmd.none
                _ -> Cmd.none

            )

        SetOpt setting ->
            ( { model | settings = updateSetting model.settings setting }
            , model.selectedOutputPort
              |> Maybe.andThen
                (flip parameterValuetoMidiMessage setting)
              |> Maybe.map midiCmd
              |> Maybe.withDefault Cmd.none
            )

        SelectOutputMidiPort midiPort -> selectOutputMidiPort (Just midiPort) model

        SelectInputMidiPort inputPort -> selectInputMidiPort (Just inputPort) model

selectOutputMidiPort outputPort model =
  case outputPort of
    Nothing -> (model, Cmd.none)
    Just midiPort ->
      ( { model | selectedOutputPort = Just midiPort }
      , Maybe.withDefault Cmd.none <| Maybe.map midiCmd (identityRequest midiPort)
      )

selectInputMidiPort inputPort model =
  case inputPort of
    Nothing -> (model, Cmd.none)
    Just midiPort ->
      ( { model | selectedInputPort = Just midiPort }
      , midiCmd (MidiMessageSubscribe { to = midiPort.id })
      )

onlyJusts : List (Maybe a) -> List a
onlyJusts x
  = case x of
      (Just y)::xs -> y :: onlyJusts xs
      (Nothing)::xs -> onlyJusts xs
      _ -> []

getParametersCmd : List Microbrute.ParameterName -> MidiPort -> Cmd Msg
getParametersCmd parameters midiPort
    = Cmd.batch
    <| List.map midiCmd
    <| onlyJusts
    <| List.map (toMidiMessage midiPort)
    <| List.map
        (Midi.Sysex << Microbrute.GetParameterValue)
    <| parameters

midiCmd = midiSender << midiMessageOutEncoder

identityRequest outputPort =
  toMidiMessage outputPort Midi.identityRequest


toMidiMessage : MidiPort -> Microbrute.MidiMessage -> Maybe MidiMessageOut
toMidiMessage { id } m
    = Microbrute.midiMessageToBytes m
    |> Maybe.andThen toByteList
    |> Maybe.map
    ( \data -> MidiMessageSend
        { to = id
        , data = data
        }
    )

parameterValuetoMidiMessage : MidiPort -> ParameterValue -> Maybe MidiMessageOut
parameterValuetoMidiMessage midiPort setting
    = Microbrute.parameterValueToMidiMessage setting
    |> Maybe.andThen (toMidiMessage midiPort)



-- VIEW


view : Model -> Html Msg
view model =
    div []
        [ label []
            [ text "Output"
            , midiPortSelector SelectOutputMidiPort model.selectedOutputPort <|
              List.filter
                  (\{ type_ } -> type_ == Output)
                  model.ports
            ]
        , label []
            [ text "Input"
            , midiPortSelector SelectInputMidiPort model.selectedInputPort <|
              List.filter
                  (\{ type_ } -> type_ == Input)
                  model.ports
            ]
        , span [] <| Maybe.withDefault [] <| Maybe.map (\x -> [ text x ]) model.midiDevice
        , div
            [ style "display" "flex"
            , style "justify-content" "space-between"
            ]
            [ sliderGroupsHtml model.settings
            ]
        , div []
            (List.map (\x -> pre [] [ text <| Debug.toString x ])
                model.settings
            )
        , div [] [ pre [] [ text <| Debug.toString model.selectedOutputPort ] ]
        , pre [] (List.map (\x -> text x.name) model.ports)
        , div [] (List.map (\x -> pre [] [text <| Debug.toString x]) <| Dict.toList model.messages)
        , pre [] (List.map (\x -> text x) model.errors)
        , optionsGroupsHtml model.settings
        , button [ onClick Decrement ] [ text "-" ]
        , div [] [ text (String.fromInt model.counter) ]
        , button [ onClick Increment ] [ text "+" ]
        ]


midiPortSelector : (MidiPort -> Msg) -> Maybe MidiPort -> List MidiPort -> Html Msg
midiPortSelector msg selected xs =
    let
        d : Dict String MidiPort
        d =
            Dict.fromList <| zip (List.map (\{ id } -> portIdToString id) xs) xs

        handleInput =
            Maybe.withDefault NoOp
                << Maybe.map msg
                << flip Dict.get
                    d

    in
    span []
        [ select
            [ onInput handleInput ]
            ( option
                 []
                 [ text "Select a MIDI interface …" ]
            :: List.map
                (\x ->
                    option
                        [ value <| portIdToString x.id
                        , Attr.selected <| (Just x) == selected
                        ]
                        [ text x.name ]
                )
                xs
            )
        ]


zip : List a -> List b -> List ( a, b )
zip =
    List.map2 Tuple.pair


flip : (c -> b -> a) -> b -> c -> a
flip fn b a = fn a b


sliderGroupsHtml : List SettingsGroup -> Html Msg
sliderGroupsHtml =
    div [] << List.map sliderGroupHtml


sliderGroupHtml : SettingsGroup -> Html Msg
sliderGroupHtml (groupName, settings) =
    fieldset [ style "display" "flex" ]
        (legend [] [ text groupName ]
            :: sliders settings
        )


sliders =
    List.map slider


optionsGroupsHtml : List SettingsGroup -> Html Msg
optionsGroupsHtml =
    div [] << List.map optionsGroupHtml


optionsGroupHtml : SettingsGroup -> Html Msg
optionsGroupHtml (groupName, settings) =
    fieldset []
        [ legend []
            [ text groupName ]
        , optionsHtml settings
        ]


optionsHtml : List Setting -> Html Msg
optionsHtml settings =
    div [] <| List.map radioButtons settings


uncurry : (a -> b -> c) -> ( a, b ) -> c
uncurry fn ( a, b ) =
    fn a b


css : List ( String, String ) -> List (Html.Attribute Msg)
css =
    List.map <| uncurry style


indexOf : List a -> a -> Int
indexOf list value =
    let
        f xs i =
            case xs of
                [] ->
                    -1

                v :: rest ->
                    if v == value then
                        i

                    else
                        f rest i + 1
    in
    f list 0


isJust : Maybe a -> Bool
isJust x =
    case x of
        Nothing ->
            False

        Just _ ->
            True


isNothing : Maybe a -> Bool
isNothing =
    not << isJust

maybeToList x =
    case x of
        Nothing ->
            []

        Just v ->
            [ v ]


slider : Setting -> Html Msg
slider (Setting { parameter, selected, values }) =
    let
        selectedValue =
            maybeToList <|
                Maybe.map (value << String.fromInt << indexOf values)
                    selected

        maxValue =
            String.fromInt <| List.length values - 1

        numberToValue =
            Maybe.andThen lookup << String.toInt

        lookup x =
            List.head <| List.drop x values

        handleChange value =
            case value of
                Nothing ->
                    NoOp

                Just x ->
                    SetOpt x
    in
    div [ style "margin" "1em 2em" ]
        [ label [ style "display" "inline-block" ]
            [ div
                [ style "display" "flex"
                , style "flex-direction" "row"
                , style "justify-content" "center"
                ]
                [ input
                    ([ type_ "range"
                     , style "appearance" "slider-vertical"
                     , style "width" "1em"
                     , Attr.min "0"
                     , Attr.max maxValue
                     , Attr.disabled <| isNothing selected
                     , onInput <| handleChange << numberToValue
                     ]
                        ++ maybeToList
                            (if List.length values <= 3 then
                                Just (style "height" "5em")

                             else
                                Nothing
                            )
                        ++ selectedValue
                    )
                    []
                , div
                    (css
                        [ ( "display", "flex" )
                        , ( "flex-direction", "column" )
                        , ( "justify-content", "space-between" )
                        ]
                    )
                    (List.map valueLabel <| List.reverse values)
                ]
            , span [] [ text <| optionName parameter ]
            ]
        ]



valueLabel : ParameterValue -> Html msg
valueLabel val =
    div [] [ text (optionValue val) ]


radioButtons : Setting -> Html Msg
radioButtons (Setting { parameter, selected, values }) =
    let
        name =
            optionName parameter
    in
    fieldset [] <|
        legend [] [ text name ]
            :: List.concatMap (createRadioButton name selected) values


createRadioButton : String -> Maybe ParameterValue -> ParameterValue -> List (Html Msg)
createRadioButton fieldSetName selected settingValue =
    let
        isChecked =
            Maybe.map ((==) settingValue) selected |> Maybe.withDefault False
    in
    [ label []
        [ input
            [ type_ "radio"
            , name fieldSetName
            , value (optionValue settingValue)
            , checked isChecked
            , onInput (\_ -> SetOpt settingValue)
            ]
            []
        , text (optionValue settingValue)
        ]
    ]
