module Microbrute exposing
    ( NotePriority (..)
    , decodeParameterName
    , microbruteSysex
    , decodeMicrobruteSysex
    , Setting (..)
    , SettingsGroup
    , MicrobruteSysex (..)
    , ParameterName (..)
    , ParameterValue (..)
    , MidiMessage
    , optionName
    , optionValue
    , settings
    , updateSetting
    , decode_
    , decode
    , encode_
    , midiMessageToBytes
    , settingToBytes
    , parameterValueToMidiMessage
    )

import BendRange exposing (BendRange)
import MidiChannel exposing (MidiChannel)
import Bytes.Decode as Decode exposing (Decoder)
import Bytes.Encode as Encode exposing (Encoder)
import Bytes exposing (Bytes, Endianness(..))
import Midi exposing (encodeMidiMessage, decodeMidiMessage, GenericMidiMessage(..), OnOff(..))
import Tuple


-- https://github.com/jmatraszek/microbrust/blob/master/src/interface/request_handler.rs
-- https://github.com/jmatraszek/microbrust/blob/master/src/interface/response_handler.rs
-- https://bulldogjob.com/news/97-how-i-reverse-engineered-synthesizer-protocol

-- Note for later: when I sent the LFO key retrigger command without the 0x01
-- byte after the counter, the Microbrute seemd to reboot into a different mode,
-- reporting its name as ‘Updater’

{-
sysex identify request:
 f0   sysex start
:7e   non realtime type
:7f   sysex channel (00 - 7f, 7f = disregard channel (= all?))
:06   subID#1 General Information
:01   subID#2 Identity request
:f7   sysex end

[0xf0, 0x7e, 0x7f, 0x06, 0x01, 0xf7]

response:
                           ^
-}


type MicrobruteSysex
  = GetParameterValue ParameterName
  | SetParameterValue ParameterName ParameterValue


type alias MidiMessage = GenericMidiMessage MicrobruteSysex

butThen : Decoder a -> Decoder () -> Decoder a
butThen a b = b |> Decode.andThen (\_ -> a)

decodeMicrobruteSysex : Decoder MicrobruteSysex
decodeMicrobruteSysex =
  twoBytesMatching 0x0501
  |> butThen oneByte
  |> Decode.andThen (\counter ->
    oneByte
    |> Decode.andThen microbruteSysex
  )

microbruteSysex : Int -> Decoder MicrobruteSysex
microbruteSysex x
  =
    case x of
      0x00 -> Decode.map GetParameterValue decodeParameterName
      0x01 -> decodeParameterName |> Decode.andThen (\paramName ->
                Decode.map
                  (SetParameterValue paramName)
                  (decodeParameterValue paramName)
                )
      _ -> Decode.fail

decodeParameterName : Decoder ParameterName
decodeParameterName =
  oneByte |> Decode.andThen (maybeToDecoder << byteToParameterName)

decodeParameterValue : ParameterName -> Decoder ParameterValue
decodeParameterValue paramName =
  oneByte |> Decode.andThen (maybeToDecoder << byteToParameterValue paramName)

maybeToDecoder x =
  case x of
    Nothing -> Decode.fail
    Just y -> Decode.succeed y

twoBytesMatching : Int -> Decoder ()
twoBytesMatching x =
  twoBytes
  |> Decode.andThen (\z -> if x == z then Decode.succeed () else Decode.fail)

oneByte = Decode.unsignedInt8

twoBytes : Decoder Int
twoBytes = Decode.unsignedInt16 BE


encodeMicrobruteSysex : MicrobruteSysex -> Maybe Encoder
encodeMicrobruteSysex message =
  let command =
        case message of
          SetParameterValue param val ->
            parameterNameToByte param |> Maybe.andThen (\parameterByte ->
            getValueByte val |> Maybe.andThen (\valueByte ->
              Just [ 0x01, parameterByte, valueByte ]
            ))
          GetParameterValue param ->
            parameterNameToByte param |> Maybe.andThen (\parameterByte ->
              Just [ 0x00, parameterByte + 1 ]
            )
  in Maybe.map msg command


msg cmd =
    let
        sysexStartByte =
            0xF0

        sysexEndByte =
            0xF7

        arturiaMidiID =
            [ 0x00
            , 0x20
            , 0x6B
            ]

        microBruteMidiID =
          [ 0x05
          , 0x01
          ]
        counter = 0x00
    in
    Encode.sequence <| List.map Encode.unsignedInt8 (
        [ sysexStartByte ]
        ++ arturiaMidiID
        ++ microBruteMidiID
        ++ [ counter ]
        ++ cmd
        ++ [ sysexEndByte ]
        )


encode_ : MidiMessage -> Maybe Encoder
encode_ = encodeMidiMessage encodeMicrobruteSysex

decode_ = decodeMidiMessage (\Midi.Arturia -> decodeMicrobruteSysex)

decode = Decode.decode decode_


type NotePriority
    = Low
    | Last
    | High


type VelocityResponse
    = Linear
    | Logarithmic
    | AntiLogarithmic


type PlayMode
    = NoteOn
    | Hold


type Retrig
    = Reset
    | Legato
    | None


type NextSequence
    = End
    | InstantReset
    | InstantContinuation


type StepMode
    = Clock
    | Gate


type StepSize
    = ⲒⳆ4
    | ⲒⳆ8
    | ⲒⳆ16
    | ⲒⳆ32


type GateLength
    = Short
    | Medium
    | Long


type SyncSource
    = External
    | Internal
    | Auto


type All
    = All


type Either a b
    = Left a
    | Right b


type ParameterValue
    -- Keyboard parameters
    = LocalControl OnOff
    | KeyboardNotePriorityValue NotePriority
    | VelocityResponse VelocityResponse
      -- Sequencer control
    | SequencerPlayMode PlayMode
    | SequencerRetrig Retrig
    | NextSequence NextSequence
    | StepMode StepMode
    | StepSize StepSize
      -- MIDI channel select
    | MidiTransmitChannel MidiChannel
    | MidiReceiveChannel (Either MidiChannel All)
      -- Module parameters
    | LfoKeyRetriggerValue OnOff
    | EnvelopeLegatoModeValue OnOff
    | BendRange BendRange
    | GateLength GateLength
    | SyncSource SyncSource

type
    ParameterName
    -- Keyboard parameters
    = KeyboardNotePriority_
    | KeyboardVelocityResponse_
    | LocalControl_
      -- Sequencer control
    | SequencerPlayMode_
    | SequencerRetrig_
    | SequencerNextSequence_
    | SequencerStepMode_
    | SequencerStepSize_
      -- MIDI channel select
    | MidiTransmitChannel_
    | MidiReceiveChannel_
      -- Module parameters
    | LfoKeyRetrigger_
    | EnvelopeLegatoMode_
    | BendRange_
    | GateLength_
    | SyncSource_


validValues : ParameterName -> List ParameterValue
validValues p =
    case p of
      KeyboardNotePriority_ -> List.map KeyboardNotePriorityValue [Low, Last, High]
      KeyboardVelocityResponse_ -> List.map VelocityResponse [ AntiLogarithmic, Logarithmic, Linear ]
      LocalControl_ -> List.map LocalControl [ Off, On ]
      SequencerPlayMode_ -> List.map SequencerPlayMode [ Hold, NoteOn ]
      SequencerRetrig_ ->  List.map SequencerRetrig [ None, Legato, Reset ]
      SequencerNextSequence_ -> List.map NextSequence [ InstantContinuation, InstantReset, End ]
      SequencerStepMode_ -> List.map StepMode [ Gate, Clock ]
      SequencerStepSize_ ->List.map StepSize [ ⲒⳆ4, ⲒⳆ8, ⲒⳆ16, ⲒⳆ32 ]
      MidiTransmitChannel_ -> List.map MidiTransmitChannel validMidiTransmitChannels
      MidiReceiveChannel_ -> List.map MidiReceiveChannel validMidiReceiveChannels
      LfoKeyRetrigger_ -> List.map LfoKeyRetriggerValue [ Off, On ]
      EnvelopeLegatoMode_ -> List.map EnvelopeLegatoModeValue [ Off, On ]
      BendRange_ -> List.map BendRange validBendRangeValues
      GateLength_ -> List.map GateLength [ Long, Medium, Short ]
      SyncSource_ -> List.map SyncSource [ Auto, Internal, External ]

validBendRangeValues : List BendRange
validBendRangeValues = List.map BendRange.fromInt <| List.range 1 12

validMidiTransmitChannels : List MidiChannel
validMidiTransmitChannels = List.map MidiChannel.fromInt <| List.range 1 16

validMidiReceiveChannels : List (Either MidiChannel All)
validMidiReceiveChannels = Right All :: List.map (Left << MidiChannel.fromInt) (List.range 1 16)

parameterName : ParameterValue -> ParameterName
parameterName v =
    case v of
      KeyboardNotePriorityValue _ -> KeyboardNotePriority_
      VelocityResponse _ -> KeyboardVelocityResponse_
      LocalControl _ -> LocalControl_
      SequencerPlayMode _ -> SequencerPlayMode_
      SequencerRetrig _ -> SequencerRetrig_
      NextSequence _ -> SequencerNextSequence_
      StepMode _ -> SequencerStepMode_
      StepSize _ -> SequencerStepSize_
      MidiTransmitChannel _ -> MidiTransmitChannel_
      MidiReceiveChannel _ -> MidiReceiveChannel_
      LfoKeyRetriggerValue _ -> LfoKeyRetrigger_
      EnvelopeLegatoModeValue _ -> EnvelopeLegatoMode_
      BendRange _ -> BendRange_
      GateLength _ -> GateLength_
      SyncSource _ -> SyncSource_

parameterGroups =
  [ ( "Keyboard Parameters"
    , [ KeyboardNotePriority_
      , KeyboardVelocityResponse_
      , LocalControl_
      ]
    )
  , ( "Sequencer Control"
    , [ SequencerPlayMode_
      , SequencerRetrig_
      , SequencerNextSequence_
      , SequencerStepMode_
      , SequencerStepSize_
      ]
    )
  , ( "MIDI Channel Select"
    , [ MidiTransmitChannel_
      , MidiReceiveChannel_
      ]
    )
  , ( "Module Parameters"
    , [ LfoKeyRetrigger_
      , EnvelopeLegatoMode_
      , BendRange_
      , GateLength_
      , SyncSource_
      ]
    )
  ]

parameters = List.concatMap (\(_, ps) -> ps) parameterGroups

type Setting = Setting
    { parameter : ParameterName
    , selected : Maybe ParameterValue
    , values : List ParameterValue
    }

flip f a b = f b a

type alias SettingsGroup = (String, List Setting)
settings : List (String, List Setting)
settings =
  let helper ps =
          ps |> List.map (\param -> Setting
              { parameter = param
              , selected = if param == LocalControl_ then Just (LocalControl On) else Nothing
              , values = validValues param
              }
            )
  in parameterGroups |> List.map (\(groupName, ps) -> (groupName, helper ps))


updateSetting : List SettingsGroup -> ParameterValue -> List SettingsGroup
updateSetting settingsGroups newValue =
    List.map (replace_ newValue) settingsGroups


replace_ : ParameterValue -> SettingsGroup -> SettingsGroup
replace_ newValue (name, settings_) =
    (name, List.map (replace newValue) settings_)


replace : ParameterValue -> Setting -> Setting
replace newValue (Setting setting) =
    if List.member newValue setting.values then
        Setting { setting | selected = Just newValue }

    else
        Setting setting


midiMessageToBytes : MidiMessage -> Maybe Bytes
midiMessageToBytes x = encode_ x |> Maybe.map Encode.encode

settingToBytes : Setting -> Maybe Bytes
settingToBytes = Maybe.map Encode.encode << settingToMidiEncoder

settingToMidiEncoder : Setting -> Maybe Encoder
settingToMidiEncoder = Maybe.andThen encode_ << settingToMidiMessage

settingToMidiMessage : Setting -> Maybe MidiMessage
settingToMidiMessage (Setting { selected, parameter }) =
    case selected of
        Nothing ->
          if parameter == LocalControl_
            then Nothing
            else Just << Sysex <| GetParameterValue parameter
        Just x -> parameterValueToMidiMessage x

parameterValueToMidiMessage : ParameterValue -> Maybe MidiMessage
parameterValueToMidiMessage x =
    case x of
        LocalControl value -> Just <| Midi.LocalControl value
        _ -> Just <| Sysex <| SetParameterValue (parameterName x) x


optionName : ParameterName -> String
optionName cmd =
    case cmd of
        LocalControl_ ->
            "Local Control"

        LfoKeyRetrigger_ ->
            "LFO Key Retrigger"

        EnvelopeLegatoMode_ ->
            "Envelope Legato Mode"

        KeyboardNotePriority_ ->
            "Note Priority"

        KeyboardVelocityResponse_ ->
            "Velocity Response"

        SequencerPlayMode_ ->
            "Play Mode"

        SequencerRetrig_ ->
            "Seq Retrig"

        SequencerNextSequence_ ->
            "Next Seq"

        SequencerStepMode_ ->
            "Step On"

        SequencerStepSize_ ->
            "Step"

        MidiTransmitChannel_ ->
            "Transmit Channel"

        MidiReceiveChannel_ ->
            "Receive Channel"

        BendRange_ ->
            "Bend Range"

        GateLength_ ->
            "Gate Length"

        SyncSource_ ->
            "Sync Source"


optionValue : ParameterValue -> String
optionValue option =
    case option of
        LocalControl Off ->
            "Off"

        LocalControl On ->
            "On"

        LfoKeyRetriggerValue Off ->
            "Off"

        LfoKeyRetriggerValue On ->
            "On"

        EnvelopeLegatoModeValue On ->
            "On"

        EnvelopeLegatoModeValue Off ->
            "Off"

        KeyboardNotePriorityValue High ->
            "High"

        KeyboardNotePriorityValue Last ->
            "Last"

        KeyboardNotePriorityValue Low ->
            "Low"

        VelocityResponse Linear ->
            "Linear"

        VelocityResponse Logarithmic ->
            "Log"

        VelocityResponse AntiLogarithmic ->
            "Antilog"

        SequencerPlayMode NoteOn ->
            "Note On"

        SequencerPlayMode Hold ->
            "Hold"

        SequencerRetrig Reset ->
            "Reset"

        SequencerRetrig Legato ->
            "Legato"

        SequencerRetrig None ->
            "None"

        NextSequence End ->
            "End"

        NextSequence InstantReset ->
            "Instant reset"

        NextSequence InstantContinuation ->
            "Instant continuation"

        StepMode Clock ->
            "Clock"

        StepMode Gate ->
            "Gate"

        StepSize ⲒⳆ4 ->
            "1⁄4"

        StepSize ⲒⳆ8 ->
            "1⁄8"

        StepSize ⲒⳆ16 ->
            "1⁄16"

        StepSize ⲒⳆ32 ->
            "1⁄32"

        MidiTransmitChannel value ->
            String.fromInt <| MidiChannel.toInt value

        MidiReceiveChannel (Left value) ->
            String.fromInt <| MidiChannel.toInt value

        MidiReceiveChannel (Right All) ->
            "All"

        BendRange value ->
            String.fromInt <| BendRange.toInt value

        GateLength Short ->
            "Short"

        GateLength Medium ->
            "Medium"

        GateLength Long ->
            "Long"

        SyncSource External ->
            "External"

        SyncSource Internal ->
            "Internal"

        SyncSource Auto ->
            "Auto"


parameterByteMapping
    = [ (LfoKeyRetrigger_, 0x0F)
      , (EnvelopeLegatoMode_, 0x0D)
      , (KeyboardNotePriority_, 0x0B)
      , (KeyboardVelocityResponse_, 0x11)
      , (SequencerPlayMode_, 0x2E)
      , (SequencerRetrig_, 0x34)
      , (SequencerNextSequence_, 0x32)
      , (SequencerStepMode_, 0x2A)
      , (SequencerStepSize_, 0x38)
      , (MidiTransmitChannel_, 0x07)
      , (MidiReceiveChannel_, 0x05)
      , (BendRange_, 0x2C)
      , (GateLength_, 0x36)
      , (SyncSource_, 0x3C)
      ]

lookupAssocList which l a =
  case l of
    [] -> Nothing
    x::xs -> if (which x == a) then Just x else lookupAssocList which xs a

lookupByFirst l a = lookupAssocList Tuple.first l a |> Maybe.map Tuple.second

lookupBySecond l a = lookupAssocList Tuple.second l a |> Maybe.map Tuple.first

parameterNameToByte : ParameterName -> Maybe Int
parameterNameToByte = lookupByFirst parameterByteMapping

byteToParameterName : Int -> Maybe ParameterName
byteToParameterName = lookupBySecond parameterByteMapping

byteToParameterValue : ParameterName -> Int -> Maybe ParameterValue
byteToParameterValue paramName byteValue =
  lookupByFirst parameterValueByteMapping paramName
  |> Maybe.andThen (flip lookupBySecond byteValue)


parameterValueByteMapping =
  [ ( KeyboardNotePriority_
    , [ ( KeyboardNotePriorityValue Last, 0x00)
      , ( KeyboardNotePriorityValue Low, 0x01)
      , ( KeyboardNotePriorityValue High, 0x02)
      ]
    )
  , ( KeyboardVelocityResponse_
    , [ (VelocityResponse Linear, 0x00)
      , (VelocityResponse Logarithmic, 0x01)
      -- swapped?
      , (VelocityResponse AntiLogarithmic, 0x02)
      ]
    )
  , ( LfoKeyRetrigger_
    , [ (LfoKeyRetriggerValue Off, 0x00)
      , (LfoKeyRetriggerValue On, 0x01)
      ]
    )
  , ( EnvelopeLegatoMode_
    , [ (EnvelopeLegatoModeValue Off, 0x00)
      , (EnvelopeLegatoModeValue On, 0x01)
      ]
    )
  , ( SequencerPlayMode_
    , [ (SequencerPlayMode Hold, 0x00)
      , (SequencerPlayMode NoteOn, 0x01)
      ]
    )
  , ( SequencerRetrig_
    , [ ( SequencerRetrig Reset, 0x00)
      , ( SequencerRetrig Legato,0x01)
      , ( SequencerRetrig None, 0x02)
      ]
    )
  , ( SequencerNextSequence_
    , [ (NextSequence End, 0x00)
      , (NextSequence InstantReset, 0x01)
      , (NextSequence InstantContinuation, 0x02)
      ]
    )
  , ( SequencerStepMode_
    , [ ( StepMode Clock, 0x00)
      , ( StepMode Gate, 0x01)
      ]
    )
  , ( SequencerStepSize_
    , [ (StepSize ⲒⳆ4, 0x04)
      , (StepSize ⲒⳆ8, 0x08)
      , (StepSize ⲒⳆ16, 0x10)
      , (StepSize ⲒⳆ32, 0x20)
      ]
    )
  , ( MidiTransmitChannel_
    , List.map
        (\n -> (MidiTransmitChannel n, MidiChannel.toInt n - 1))
        validMidiTransmitChannels
    )
  , ( MidiReceiveChannel_
    , List.map
        (\n ->
            ( MidiReceiveChannel n
            , case n of
                  Right All ->
                      0x10
                  Left midiChannel ->
                      MidiChannel.toInt midiChannel - 1
            )
        )
        validMidiReceiveChannels
    )
  , ( BendRange_
    , List.map
        (\x -> (BendRange x, BendRange.toInt x))
        validBendRangeValues
    )
  , ( GateLength_
    , [ ( GateLength Short, 0x01)
      , ( GateLength Medium, 0x02)
      , ( GateLength Long, 0x03)
      ]
    )
  , ( SyncSource_
    , [ ( SyncSource Auto, 0x00)
      , ( SyncSource Internal, 0x01)
      , ( SyncSource External, 0x02)
      ]
    )
  ]


getValueByte : ParameterValue -> Maybe Int
getValueByte setting =
  case setting of
    LocalControl _ -> Nothing
    x -> lookupByFirst (List.concatMap Tuple.second parameterValueByteMapping) x
